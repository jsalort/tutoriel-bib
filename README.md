# tutoriel-bib

Tutoriel pour lier Google Scholar à BibCnrs et la Bibliothèque Diderot de Lyon.
Le tutoriel est accessible via Gitlab Pages à [cette adresse](http://jsalort.frama.io/tutoriel-bib).

Les transparents s'appuient sur [Reveal.js](https://revealjs.com/#/).
